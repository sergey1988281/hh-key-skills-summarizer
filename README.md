# HeadHunter key skills summarizer web application
This project main purpose is to help candidates in determining key skills for certain positions parsing HeadHunter.ru public API and performing its summarization.
Another useful feature of the project is to show on real example value from asynchronous operation and caching. Project leverages requests library for doing
synchronous fetching and aiohttp, aiofiles for asynchronous ones. 

Used technologies: ASYNCIO, AIOHTTP, AIOFILES, AIOREDIS, REDIS, FASTAPI, LOGURU.

## BUILD:
Run in local docker: docker-compose up  
Run in kubernetes: helm install... -n <namespace>. Please check and edit .helm/values.yaml before then create a namespace. Also you will need to add DNS record according to ingress public IP and provide tls certificates.

## USAGE:
Local docker: After build open web browser and follow: <ip of your docker>:8000  
Kubernetes: Open DNS name on port 80 or 443 if you have enabled tls in ingress (by default enabled)

## PIPELINE:
This project has following pipeline setup:
1. On every commit with change to summarizer folder it perform checks for linting and running unit test cases as well as static application security analisys of python code.
2. On every commit with change to .helm folder it performs static application security analisys for kubernetes manifests.
3. On every commit with change to summarizer folder, Dockerfile or .dockerignote files it does above and rebuilds an image then scans it for vulnerabilities then image pushed to container registry.
4. On every commit to Development branch it deploys application to development kubernetes cluster environment then runs functional tests in chrome browser.
5. On every tag it does all above plus deploys application to production kubernetes cluster environment.