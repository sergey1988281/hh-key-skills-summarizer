FROM python:3.10-alpine
RUN addgroup -g 998 python && adduser -S -u 998 -g python python
RUN mkdir /usr/app && chown python:python /usr/app
WORKDIR /usr/app
COPY requirements.txt .
RUN pip3 install -r requirements.txt && rm requirements.txt
COPY --chown=python:python ./summarizer ./summarizer
USER 998
EXPOSE 8000
ENTRYPOINT ["python", "-m", "summarizer"]