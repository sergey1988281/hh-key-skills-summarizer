from seleniumbase import BaseCase


class finctionalTest(BaseCase):
    def test_basic_search(self):
        self.open("https://dev.hh-summarizer.com")
        self.assert_text("Welcome to HH key skills summarizer service", "h1")
        self.type("#search_query", "DevOps Engineer")
        self.click("#clickme")
        self.assert_text("Results for: DevOps Engineer", "h1")

    def test_advanced_search(self):
        self.open("https://dev.hh-summarizer.com")
        self.assert_text("Welcome to HH key skills summarizer service", "h1")
        self.type("#search_query", "DevOps AND Engineer")
        self.check_if_unchecked("#advanced_search")
        self.click("#clickme")
        self.assert_text("Results for: DevOps AND Engineer", "h1")
