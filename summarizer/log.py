from loguru import logger
from fastapi import Request
from .settings import settings


logger.add(settings.log_file, format=settings.log_format)


def log_msg(request: Request, message: str) -> str:
    """Forms a log message having http request data

    Args:
        request (Request): HTTP Request
        message (str): Log mesage

    Returns:
        str: string containing HTTP request data and log message
    """
    return (f" {str(request.client.host)} "
            f"{str(request.headers.get('User-Agent'))} "
            f"{str(request.method)} {message}")
