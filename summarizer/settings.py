from pydantic import BaseSettings


class Settings(BaseSettings):
    # HTTP server settings
    server_host: str = "0.0.0.0"
    server_port: int = 8000
    server_log_level: str = "debug"
    # Cache server settings
    cache_host: str = "0.0.0.0"
    cache_port: int = 6379
    # HTML files settings
    static_folder: str = "summarizer/static"
    templates_folder: str = "summarizer/templates"
    # Logger settings
    log_file: str = "summarizer.log"
    log_format: str = "{time} - {name} - {level} - {message}"


settings = Settings(
    _env_file=".env",
    _env_file_encoding="utf-8"
)
