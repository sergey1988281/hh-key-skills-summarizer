from fastapi import APIRouter
from fastapi import Request, Depends
from fastapi.responses import StreamingResponse
from ..log import logger, log_msg
from ..services.hh_vacancy_summary_service import HHVacancySummaryService


router = APIRouter()


@router.get("/export")
async def export_csv(
            search_query: str,
            volume: int,
            location: str,
            request: Request,
            summary_service: HHVacancySummaryService = Depends()
):
    """Exports key skills top to CSV file
    Args:
        search_query (str): Filters to determine vacancy.
        volume (int): How many vacancies to summarize.
        location (str): filter vacancies region.
        request (Request): HTTP request data
        summary_service (HHVacancySummaryService): Summary fetcher class
        Gets from FastAPI dependency.
    """
    logger.info(log_msg(request, (f"Key skills top export requested"
                                  f"f{search_query=}, {volume=}, {location=}"
                                  )))
    report = await summary_service.export_csv(
        search_query, volume, location
    )
    return StreamingResponse(
        report,
        media_type="text/csv",
        headers={
            "Content-Disposition": f"attachment; filename={search_query}.csv"
        }
    )
