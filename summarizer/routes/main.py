from fastapi import APIRouter
from fastapi import Request, Form, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from time import perf_counter
from ..settings import settings
from ..log import logger, log_msg
from ..services.hh_vacancy_summary_service import HHVacancySummaryService


router = APIRouter()
templates = Jinja2Templates(directory=settings.templates_folder)


@router.get("/", response_class=HTMLResponse)
async def index(request: Request):
    """Main page route renders welcome message and request form
    Args:
        request (Request): HTTP request details, injected by fastapi
    """
    logger.info(log_msg(request, "Main page opened"), enqueue=True)
    title = "Welcome to HH key skills summarizer service"
    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "title": title,
                                       }
                                      )


@router.post("/", response_class=HTMLResponse)
async def summary(request: Request,
                  search_query: str = Form(),
                  volume: int = Form(),
                  location: str = Form(),
                  asynchronous: bool = Form(True),
                  advanced_search: bool = Form(False),
                  no_cache: bool = Form(False),
                  summary_service: HHVacancySummaryService = Depends()):
    """Renders main page plus requested data
    Args:
        request (Request): HTTP request details, injected by fastapi
        search_query (str): Filters to determine vacancy.
        Gets from http Form().
        volume (int): How many vacancies to summarize.
        Gets from http Form().
        location (str): filter vacancies region.
        Gets from http Form().
        asynchronous (bool): Specify how to fetch symc or async.
        Gets from http Form().
        advanced_search (bool): Specify if advanced search is used or not.
        Gets from http Form().
        no_cache (bool): Force skip cache lookup even though value was cached.
        Gets from http Form().
        summary_service (HHVacancySummaryService): Summary fetcher class
        Gets from FastAPI dependency.
    """
    logger.info(log_msg(request, (f"Requested: {search_query}, for {volume} "
                                  f"vacancies, in {location} using advanced "
                                  f"search {advanced_search} without cache "
                                  f"{no_cache} asynchronous {asynchronous}")),
                enqueue=True)
    title = f"Results for: {search_query}"
    start = perf_counter()
    data = await summary_service.get_key_skills_top(
        search_query=search_query,
        volume=volume,
        location=location,
        asynchronous=asynchronous,
        advanced_search=advanced_search,
        no_cache=no_cache
    )
    end = perf_counter()
    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "title": title,
                                       "elapsed": round(end - start, 4),
                                       "body": data,
                                       "search_query": search_query,
                                       "volume": volume,
                                       "location": location,
                                       }
                                      )


@router.get("/reset", response_class=HTMLResponse)
async def reset(request: Request,
                summary_service: HHVacancySummaryService = Depends()):
    """Clears cache
    Args:
        request (Request):  HTTP request details, injected by fastapi
        summary_service (HHVacancySummaryService): Summary fetcher class
        Gets from FastAPI dependency.
    """
    logger.info(log_msg(request, "Cache reset requested"), enqueue=True)
    title = "Cache has been reset, enter new query"
    await summary_service.reset_cache()
    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "title": title,
                                       }
                                      )
