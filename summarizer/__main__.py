import uvicorn
from .settings import settings
from .log import logger


# Run application server
logger.info("Starting application")
uvicorn.run(
    "summarizer.app:app",
    host=settings.server_host,
    port=settings.server_port,
    log_level=settings.server_log_level,
    proxy_headers=True,
    forwarded_allow_ips="*"
    )
