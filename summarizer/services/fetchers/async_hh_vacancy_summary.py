import asyncio
import aiohttp
import aiofiles
import os
import time
import json
from datetime import datetime
from collections import Counter
try:
    from .vacancy import Vacancy
except ImportError:
    from vacancy import Vacancy


class AsyncHHVacancySummary:
    """Retrieves and provides different summary of HeadHunter
    vacancies given specific request, based on public api:
    https://github.com/hhru/api/blob/master/docs/general.md
    """
    def __init__(self,
                 search_query: str,
                 volume: int = 500,
                 num_pages: int = 5,
                 location: str = "Russia",
                 limit_concurency: int = 50
                 ) -> None:
        """Initializes HH Vacancy Summary class
        Args:
            search_query (str): Vacancy query keywords supports
            query language: https://hh.ru/article/1175
            volume (int, optional): Maximum number of vacancies to summarize
            (might be less). Defaults to 500.
            num_pages (int, optional): Splits catalog to pages, more pages
            more parralelism. Defaults to 5.
            location (str, optional): Location to limit search.
            Defaults to "Russia".
            limit_concurency (int, optional): Limits concurency to certain
            number. Defaults to 50.
        """
        self._search_query = search_query
        self._num_pages = num_pages
        self._per_page = volume // num_pages
        self._url = "https://api.hh.ru/vacancies"
        locations = {
            "Moscow": 1,
            "Russia": 113
        }
        self._location = locations[location]
        timestamp = datetime.now().strftime("%d%m%Y%H%M%S")
        self._joblist_path = f"./docs/{timestamp}/joblists"  # Job lists folder
        self._jobs_path = f"./docs/{timestamp}/jobs"  # Specific vacancy folder
        self._limit_concurency = limit_concurency

    @staticmethod
    async def _fetch_page(URL: str,
                          limiter: asyncio.locks.Semaphore,
                          params: dict[str, str | int] | None = None
                          ) -> str:
        """Downloads data from api
        Args:
            URL (str): url of page to download
            limiter(Semaphore): Limits level of concurency
            params (dict, optional): HTTP GET request params. Defaults to None.
        Returns:
            str: Json as a string
        """
        async with aiohttp.ClientSession() as session, limiter:
            responce = await session.get(URL, params=params)
            text = await responce.text()
        return text

    @staticmethod
    async def _save_file(filename: str,
                         text: str,
                         limiter: asyncio.locks.Semaphore
                         ) -> None:
        """Generates a file locally
        Args:
            filename (str): Name of file to persist
            text (str): File content
            limiter(Semaphore): Limits level of concurency
        """
        json_object = json.loads(text)
        async with aiofiles.open(filename, "w", encoding="utf-8") as file, \
                limiter:
            await file.write(json.dumps(json_object, ensure_ascii=False))

    @staticmethod
    async def _extract_vacancy_urls(file_path: str,
                                    limiter: asyncio.locks.Semaphore
                                    ) -> list[Vacancy]:
        """Extracts individual vacancies details from job list
        Args:
            file_path (str): path to joblist file
            limiter(Semaphore): Limits level of concurency
        Returns:
            list[Vacancy]: Individual vacancy
        """
        async with aiofiles.open(file_path, encoding='utf8') as file, limiter:
            jsonText = await file.read()
        jsonObj = json.loads(jsonText)
        return [Vacancy(item["id"], item["url"]) for item in jsonObj['items']]

    @staticmethod
    def _ensure_path(path: str) -> None:
        """Ensure directory exist and creates if not
        Args:
            path (str): Path to directory
        """
        if not os.path.exists(path):
            os.makedirs(path)

    async def _fetch_vacancies(self) -> int:
        """Orchestrates data capturing async process tasks and setup everything
        Returns:
            int: Number of vacancies fetched
        """
        self._ensure_path(self._joblist_path)
        self._ensure_path(self._jobs_path)
        limiter = asyncio.Semaphore(self._limit_concurency)
        params_list = [{
                        "text": self._search_query,
                        "area": self._location,
                        "per_page": self._per_page,
                        "page": page,
                        } for page in range(self._num_pages)
                       ]
        fetch_tasks = [self._fetch_page(self._url, limiter, params)
                       for params in params_list]
        docs = await asyncio.gather(*fetch_tasks)
        filenames = [self._joblist_path + f"/{page}.json"
                     for page in range(self._num_pages)]
        save_tasks = [self._save_file(filename, text, limiter)
                      for filename, text in zip(filenames, docs)]
        await asyncio.gather(*save_tasks)
        extract_tasks = [self._extract_vacancy_urls(filename, limiter)
                         for filename in filenames]
        vacancy_sublists = await asyncio.gather(*extract_tasks)
        vacancies = [item
                     for sublist in vacancy_sublists
                     for item in sublist
                     ]  # This just flatten list of lists
        fetch_tasks = [self._fetch_page(vacancy.url, limiter)
                       for vacancy in vacancies]
        docs = await asyncio.gather(*fetch_tasks)
        save_tasks = [self._save_file(
                        self._jobs_path + f"/{vacancy.id}.json",
                        doc,
                        limiter)
                      for vacancy, doc in zip(vacancies, docs)
                      ]
        await asyncio.gather(*save_tasks)
        return len(vacancies)

    @staticmethod
    async def _extract_key_skills(vacancy_path: str,
                                  limiter: asyncio.locks.Semaphore
                                  ) -> list[str]:
        """Given a vacancy path extracts all key skills out of it
        in a form of list
        Args:
            vacancy_path (str): Path to a file containing vacancy
            limiter(Semaphore): Limits level of concurency
        Returns:
            list[str]: List of key skills for a given vacancy
        """
        async with aiofiles.open(vacancy_path, encoding='utf8') as file, \
                limiter:
            jsonText = await file.read()
        jsonObj = json.loads(jsonText)
        try:
            return [skill["name"] for skill in jsonObj["key_skills"]]
        except KeyError:
            print(f"Error: {vacancy_path}")
            return []

    async def _build_skills_top(self) -> list[tuple[str, int]]:
        """Extracts key skills from all vacancies asynchroniously and builds
        a summary returns ordered list of skills and occurencies
        Returns:
            list[tuple[str, int]]: Ordered list of skills by their occurencies
        """
        limiter = asyncio.Semaphore(self._limit_concurency)
        filenames = [f"{self._jobs_path}/{vacancy_file}"
                     for vacancy_file in os.listdir(self._jobs_path)]
        extract_tasks = [self._extract_key_skills(filename, limiter)
                         for filename in filenames]
        key_skills_sublists = await asyncio.gather(*extract_tasks)
        key_skills = [item
                      for sublist in key_skills_sublists
                      for item in sublist
                      ]  # This just flatten list of lists
        top = Counter(key_skills)
        return top.most_common()

    async def get_skills_top(self) -> tuple[int, list[tuple[str, int]]]:
        """Provides skills top from Headhunter given a request details
        Returns:
            tuple[int, list[tuple[str, int]]]: Count of summarized vacancies
            appended by skills top.
        """
        count_vacancies = await self._fetch_vacancies()
        skills_top = await self._build_skills_top()
        return (count_vacancies, skills_top)


if __name__ == "__main__":
    # This block to run fetcher from command line
    while True:
        try:
            search_query = input("Enter search query:")
            volume = int(input("Enter volume of vacancies to process:"))
            location = input("Enter desired location: [Moscow/Russia]")
            break
        except ValueError:
            print("Incorrect input value, try again. Precc Ctrl + C to exit.")
    HHVacancies = AsyncHHVacancySummary(search_query=search_query,
                                        volume=volume,
                                        location=location)
    start = time.perf_counter()
    print(asyncio.run(HHVacancies.get_skills_top()))
    end = time.perf_counter()
    print(end - start)
