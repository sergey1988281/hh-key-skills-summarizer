import requests
import os
import time
import json
from datetime import datetime
from collections import Counter
try:
    from .vacancy import Vacancy
except ImportError:
    from vacancy import Vacancy


class SyncHHVacancySummary:
    """Retrieves and provides different summary of HeadHunter
    vacancies given specific request, based on public api:
    https://github.com/hhru/api/blob/master/docs/general.md
    """
    def __init__(self,
                 search_query: str,
                 volume: int = 500,
                 num_pages: int = 5,
                 location: str = "Russia"
                 ) -> None:
        """Initializes HH Vacancy Summary class
        Args:
            search_query (str): Vacancy query keywords supports
            query language: https://hh.ru/article/1175
            volume (int, optional): Maximum number of vacancies to summarize
            (might be less). Defaults to 500.
            num_pages (int, optional): Splits catalog to pages, more pages
            more parralelism. Defaults to 5.
            location (str, optional): Location to limit search.
            Defaults to "Russia".
        """
        self._search_query = search_query
        self._num_pages = num_pages
        self._per_page = volume // num_pages
        self._url = "https://api.hh.ru/vacancies"
        locations = {
            "Moscow": 1,
            "Russia": 113
        }
        self._location = locations[location]
        timestamp = datetime.now().strftime("%d%m%Y%H%M%S")
        self._joblist_path = f"./docs/{timestamp}/joblists"  # Job lists folder
        self._jobs_path = f"./docs/{timestamp}/jobs"  # Specific vacancy folder

    @staticmethod
    def _fetch_page(URL: str,
                    params: dict[str, str | int] | None = None
                    ) -> str:
        """Downloads data from api
        Args:
            URL (str): url of page to download
            params (dict, optional): HTTP GET request params. Defaults to None.
        Returns:
            str: Json as a string
        """
        with requests.Session() as session:
            responce = session.get(URL, params=params)
            text = responce.content.decode()
        return text

    @staticmethod
    def _save_file(filename: str, text: str) -> None:
        """Generates a file locally
        Args:
            filename (str): Name of file to persist
            text (str): File content
        """
        json_object = json.loads(text)
        with open(filename, "w", encoding="utf-8") as file:
            file.write(json.dumps(json_object, ensure_ascii=False))

    @staticmethod
    def _extract_vacancy_urls(joblist_path: str) -> list[Vacancy]:
        """Extracts individual vacancies details from job list
        Args:
            file_path (str): path to joblist file
        Returns:
            list[Vacancy]: Individual vacancy
        """
        with open(joblist_path, encoding='utf8') as file:
            jsonText = file.read()
        jsonObj = json.loads(jsonText)
        return [Vacancy(item["id"], item["url"]) for item in jsonObj['items']]

    @staticmethod
    def _ensure_path(path: str) -> None:
        """Ensure directory exist and creates if not
        Args:
            path (str): Path to directory
        """
        if not os.path.exists(path):
            os.makedirs(path)

    def _fetch_vacancies(self) -> int:
        """Orchestrates data capturing process tasks and setup everything
        Returns:
            int: Number of vacancies fetched
        """
        self._ensure_path(self._joblist_path)
        self._ensure_path(self._jobs_path)
        joblist_filenames = [self._joblist_path + f"/{page}.json"
                             for page in range(self._num_pages)
                             ]
        params_list = [{
                        "text": self._search_query,
                        "area": self._location,
                        "per_page": self._per_page,
                        "page": page,
                        } for page in range(self._num_pages)
                       ]
        [self._save_file(filename, self._fetch_page(self._url, params))
         for filename, params in zip(joblist_filenames, params_list)
         ]
        total_vacancies = 0
        for joblist in joblist_filenames:
            vacancies = self._extract_vacancy_urls(joblist)
            total_vacancies += len(vacancies)
            [self._save_file(self._jobs_path + f"/{vacancy.id}.json",
                             self._fetch_page(vacancy.url)
                             )
             for vacancy in vacancies
             ]
        return total_vacancies

    @staticmethod
    def _extract_key_skills(vacancy_path: str) -> list[str]:
        """Given a vacancy path extracts all key skills out of it
        in a form of list
        Args:
            vacancy_path (str): Path to a file containing vacancy
        Returns:
            list[str]: List of key skills for a given vacancy
        """
        with open(vacancy_path, encoding='utf8') as file:
            jsonText = file.read()
        jsonObj = json.loads(jsonText)
        try:
            return [skill["name"] for skill in jsonObj["key_skills"]]
        except KeyError:
            print(f"Error: {vacancy_path}")
            return []

    def _build_skills_top(self) -> list[tuple[str, int]]:
        """Extracts key skills from all vacancies and builds a summary
        returns ordered list of skills and occurencies
        Returns:
            list[tuple[str, int]]: Ordered list of skills by their occurencies
        """
        top = Counter()
        for vacancy_file in os.listdir(self._jobs_path):
            top.update(
                self._extract_key_skills(f"{self._jobs_path}/{vacancy_file}")
            )
        return top.most_common()

    def get_skills_top(self) -> tuple[int, list[tuple[str, int]]]:
        """Provides skills top from Headhunter given a request details
        Returns:
            tuple[int, list[tuple[str, int]]]: Count of summarized vacancies
            appended by skills top.
        """
        count_vacancies = self._fetch_vacancies()
        skills_top = self._build_skills_top()
        return (count_vacancies, skills_top)


if __name__ == "__main__":
    # This block to run fetcher from command line
    while True:
        try:
            search_query = input("Enter search query:")
            volume = int(input("Enter volume of vacancies to process:"))
            location = input("Enter desired location: [Moscow/Russia]")
            break
        except ValueError:
            print("Incorrect input value, try again. Precc Ctrl + C to exit.")
    HHVacancies = SyncHHVacancySummary(search_query=search_query,
                                       volume=volume,
                                       location=location)
    start = time.perf_counter()
    print(HHVacancies.get_skills_top())
    end = time.perf_counter()
    print(end - start)
