import json
from typing import Any
from csv import DictWriter
from io import StringIO
from aioredis.client import Redis
from aioredis.exceptions import ConnectionError
from fastapi import Depends
from ..cache import get_cache
from ..log import logger
from .fetchers.async_hh_vacancy_summary import AsyncHHVacancySummary
from .fetchers.sync_hh_vacancy_summary import SyncHHVacancySummary


class HHVacancySummaryService:
    """Manages communication with cache and fetchers
    """
    def __init__(self,
                 cache: Redis = Depends(get_cache)
                 ) -> None:
        """Initializes headhunter vacancy summarizer service
        Args:
            cache (Redis, optional): Redis cache object.
            Defaults to Depends(get_cache).
        """
        self.cache = cache

    async def get_key_skills_top(
                  self,
                  search_query: str,
                  volume: int,
                  location: str,
                  asynchronous: bool = True,
                  advanced_search: bool = False,
                  no_cache: bool = False) -> tuple[int, list[tuple[str, int]]]:
        """Based un user input gets required data either from
        cache or from sync/async fetchers
        Args:
            search_query (str): Filters to determine vacancy.
            volume (int): How many vacancies to summarize.
            location (str): filter vacancies region.
            asynchronous (bool): Aspecify how to fetch async or sync.
            advanced_search (bool): Specify if advanced syntax is used.
            no_cache (bool): Specify if bypass cache or not.
        Returns:
            tuple[int, list[tuple[str, int]]]: Number of summarized vacancies
            appended by skills top.
        """
        if not advanced_search:
            search_query = " AND ".join(search_query.split())
        cache_key = " ".join([search_query, str(volume), location])
        if not no_cache:
            cahced_data = None
            try:
                cahced_data = await self.cache.get(cache_key)
            except ConnectionError as err:
                logger.exception("Cache server is not available:", err)
            if cahced_data:
                logger.info(f"{cache_key} data been retrieved from cache")
                return json.loads(cahced_data)
        if asynchronous:
            fetcher = AsyncHHVacancySummary(
                search_query=search_query,
                volume=volume,
                location=location
            )
            data = await fetcher.get_skills_top()
        else:
            fetcher = SyncHHVacancySummary(
                search_query=search_query,
                volume=volume,
                location=location
            )
            data = fetcher.get_skills_top()
        data_json = json.dumps(data)
        try:
            await self.cache.set(cache_key, data_json)
            logger.info(f"{cache_key} been fetched, summarized "
                        f"and inserted to cache")
        except ConnectionError as err:
            logger.exception("Cache server is not available:", err)
        return data

    async def reset_cache(self) -> None:
        """Compeletely clears all entries from cache
        """
        try:
            keys = await self.cache.keys('*')
            if keys:
                await self.cache.delete(*keys)
        except ConnectionError as err:
            logger.exception("Cache server is not available:", err)

    async def export_csv(self,
                         search_query: str,
                         volume: int,
                         location: str,
                         ) -> Any:
        """Exports a file, containing top of skills
        for specific search query
        Args:
            search_query (str): Filters to determine vacancy.
            volume (int): How many vacancies to summarize.
            location (str): filter vacancies region.
        Returns:
            Any: in-memory file like object, contaiing list of operations
        """
        output = StringIO()
        writer = DictWriter(
            output,
            fieldnames=['Key skill', 'Sum demand'],
            extrasaction="ignore"
        )
        _, key_skills_top = await self.get_key_skills_top(
            search_query,
            volume,
            location,
            True)
        writer.writeheader()
        for key_skill in key_skills_top:
            writer.writerow({'Key skill': key_skill[0],
                             'Sum demand': key_skill[1]})
        output.seek(0)
        return output
