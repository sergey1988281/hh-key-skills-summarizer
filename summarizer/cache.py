import aioredis
from aioredis.client import Redis
from .settings import settings

url = f"redis://{settings.cache_host}:{settings.cache_port}"


async def get_cache() -> Redis:
    """Dependency injection
    Returns:
        Redis: cache obkect
    Yields:
        Iterator[Session]: sqlalchemy session object
    """
    redis = aioredis.from_url(url, decode_responses=True)
    try:
        yield redis
    finally:
        await redis.close()
