from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from .routes import router
from .settings import settings
from prometheus_fastapi_instrumentator import Instrumentator


app = FastAPI()
app.mount("/static",
          StaticFiles(directory=settings.static_folder),
          name="static")
app.include_router(router)


@app.on_event("startup")
async def startup():
    instrumentator = Instrumentator(
        excluded_handlers=["/reset", "/metrics"],
    )
    instrumentator.instrument(app).expose(app)
